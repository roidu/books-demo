# Books test

based on: https://testdriven.io/blog/running-flask-on-kubernetes/

## UI test task

The test reflects real situation when REST API changes with the new property (rating in this case). Implement UI changes to utilize this.

GOAL:

1. Create UI to have rating included with the books
2. /books2 api is already implemented for you, so change the UI to use that endpoint (instead of /books)
3. and create UI to show rating while listing books
4. allow update rating on the update modal

Optional:

1. Improve styling from the baseline
2. Improve UX from the baseline

### Book entity in JSON:

http://35.228.6.214:5000/books2

``
{
  author: "Jack Kerouac",
  id: 1,
  rating: 1
  read: true,
  title: "On the Road"
}
``

You may see the detailed REST APIs from server dir (books.py vs books2.py). There are typical PUT, GET, DELETE...

## Dev env setup

### Prerequisities

Node.js installed

Tested with:

* v10.15.3, npm 6.4.1 on Windows 10
* v11.3.0, npm 6.4.1 on Mac
* v8.10.0, npm 3.5.2 on Linux

### Development 

To start UI development on local machine (Mac/Linux), do:

    cd client
    npm install
    ROOT_API=http://35.228.6.214:5000 npm run dev
    
For windows

    $env:ROOT_API="http://35.228.6.214:5000"
    npm run dev
    
Edit files with the editor/IDE of your choice (See goals what is expected):

browse to:

    localhost:8080

...in order to see changes real-time.

## When ready

send files (only the changed) via email to alex.jantunen@roidu.com.
 
To see what is changed on local filesystem, do:

    git status