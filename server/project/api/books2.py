import os

from flask import Blueprint, jsonify, request

from project.api.models import Book2
from project import db


books2_blueprint = Blueprint('books2', __name__)


@books2_blueprint.route('/books2', methods=['GET', 'POST'])
def all_books():
    response_object = {
        'status': 'success',
        'container_id': os.uname()[1]
    }
    if request.method == 'POST':
        post_data = request.get_json()
        title = post_data.get('title')
        author = post_data.get('author')
        read = post_data.get('read')
        db.session.add(Book2(title=title, author=author, read=read, rating=0))
        db.session.commit()
        response_object['message'] = 'Book added!'
    else:
        response_object['books'] = [book.to_json() for book in Book2.query.all()]
    return jsonify(response_object)


@books2_blueprint.route('/books2/ping', methods=['GET'])
def ping():
    return jsonify({
        'status': 'success',
        'message': 'pong!',
        'container_id': os.uname()[1]
    })


@books2_blueprint.route('/books2/<book_id>', methods=['PUT', 'DELETE'])
def single_book(book_id):
    response_object = {
      'status': 'success',
      'container_id': os.uname()[1]
    }
    book = Book2.query.filter_by(id=book_id).first()
    if request.method == 'PUT':
        post_data = request.get_json()
        book.title = post_data.get('title')
        book.author = post_data.get('author')
        book.read = post_data.get('read')
        book.rating = post_data.get('rating')
        db.session.commit()
        response_object['message'] = 'Book updated!'
    if request.method == 'DELETE':
        db.session.delete(book)
        db.session.commit()
        response_object['message'] = 'Book removed!'
    return jsonify(response_object)


if __name__ == '__main__':
    app.run()
